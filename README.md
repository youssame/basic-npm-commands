# BASIC NPM COMMANDS

## What is NPM?
is a Node Package Manager that comes bundled with it and aids every Node development.

## Install NPM?
[https://nodejs.org/en/](https://nodejs.org/en/)

## Check NPM version?
    npm -v
    npm --version

## Init an NPM project?
    npm init //you can create your project infos
    npm init -y   //or --yes we are not hav to answer question

## Install an NPM package?

    npm install PACKAGE_NAME@4.2.5
    npm i PACKAGE_NAME 
    npm i -g PACKAGE_NAME 
    //to install a package globally in the computer, 
    //use npm root -g t osee the location
    npm i PACKAGE_NAME --save-dev //to use the package only in dev environnement

## update an NPM package?

    npm update PACKAGE_NAME

## Use an NPM package?
In the js file: 
   
     const variable = require('package_name');
     variable.<the module work> // GOD i'm awesome in writing LOL

## NPM package version?

![ScreenShot](https://cdncontribute.geeksforgeeks.org/wp-content/uploads/semver.png)
     

##  Scripts in NPM package.json file

    "script": {
    	"start": "node app.js" // when you execute 'npm start', 'node app.js' will be executed.
    }

##  Remove package.json file

    npm remove PACKAGE_NAME -g
    npm remove PACKAGE_NAME
